//
// Created by brand on 25. 12. 2022.
//
#include "stdio.h"
#include <iostream>
#include <queue>
#include <thread>
#include "clients/UrlParser.h"


#include "clients/HttpClient.h"
#include "clients/FtpClient.h"
#include "clients/FtpsClient.h"
#include "clients/BitTorrentClient.h"
#include "DownloadHandler.h"
#include "FIleManger.h"
#define HTTP_PROTOCOL 1
#define FTP_PROTOCOL 2
#define BIT_TORRENT_PROTOCOL 3
#define FTPS_PROTOCOL 4

#define MAX_ACTIVE_DOWNLOAD_THREADS 2    //Nadstavenie maximalneho poctu aktivnych stahovani

#ifndef SEMESTRALKA_DOWNLOADMANAGER_H
#define SEMESTRALKA_DOWNLOADMANAGER_H

using namespace std;

const int LENGTH_PREPARED_URLS = 7;

class DownloadManager {
public:
    DownloadManager();
    ~DownloadManager();
    void startManager();
private:
    string preparedUrls[LENGTH_PREPARED_URLS] = {
        "http://speedtest.tele2.net/1MB.zip",
        "https://www.dundeecity.gov.uk/sites/default/files/publications/civic_renewal_forms.zip",
        "ftp://w23726_testftp:kPQARvxn@23726.w26.wedos.net/test.jpg",
        "ftp://demo:password@test.rebex.net/readme.txt",
        "ftps://demo:password@test.rebex.net/readme.txt",
        "https://www.dundeecity.gov.uk/sites/default/files/publications/civic_renewal_forms.zip",
        "ftp://w23726_testftp:kPQARvxn@23726.w26.wedos.net/aaa/100mb.bin",
    };
    bool downloadLoopRun = true;
    int activeThreads = 0;
    vector<thread>* activeThreadsContainer;
    priority_queue<DownloadHandler>* downloadQueue;        // classic downloads
    priority_queue<DownloadHandler>* plannedDownloadQueue; // downloads planned on time
    shared_ptr<Logger> logger_;
    string historyLogFilePath;
    FIleManager* fileManager;
    mutex downloadQueueMutex;
    mutex activeThreadsMutex;
    mutex loggerMutex;
    mutex downloadLoopMutex;

private:
    void httpDownload(const string & url);
    void ftpDownload(const string & url);
    void ftpsDownload(const string & url);
    void torrentDownload(const string & url);
    void download(const string& url);
    int chooseProtocol(const string& url);
    string offerUrl();
    void startDownloadingLoop();
    time_t createTime();
};


#endif //SEMESTRALKA_DOWNLOADMANAGER_H
