//
// Created by samuel on 4.1.2023.
//

#include "Logger.h"

Logger::Logger(const string &fileName) {
    this->fileName = fileName;
}

void Logger::writeMessage(const string &message) {
    string now = getCurrentTime();
    string logMessage = now + "\t" + message;
//    cout << "Downloaded: %s" << logMessage.c_str() << endl;
    this->loggerMutex.lock();
    this->outFile.open(this->fileName, ios_base::app);
    this->outFile << logMessage << endl;
    this->loggerMutex.unlock();
    outFile.close();
}

string Logger::getCurrentTime() {
    time_t now = time(nullptr);
    struct tm tstruct{};
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
    return string(buf);
}

void Logger::printContentOfFile() {
    ifstream file(fileName);

    cout << "File location: " << fileName << endl;
    if (file.is_open()) {
        cout << file.rdbuf() << endl;
    }
}

void Logger::debugPrint(const string &message) {
    if (DEBUG) {
        cout << message << endl;
    }
}
