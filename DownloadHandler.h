//
// Created by brand on 4. 1. 2023.
//


#ifndef SEMESTRALKA_DOWNLOADHANDLER_H
#define SEMESTRALKA_DOWNLOADHANDLER_H
#include "clients/HttpClient.h"
#include "clients/FtpClient.h"

#include "Logger.h"
#include <string>
#include <ctime>

#define HTTP_PROTOCOL 1
#define FTP_PROTOCOL 2
#define BIT_TORRENT_PROTOCOL 3
#define FTPS_PROTOCOL 4

using namespace std;
class DownloadHandler {
private:
    string url;
    int priority;
    time_t startDownloadTime;
    bool startTimeIsSet;



public:
    DownloadHandler(string url, int priority, time_t plannedTime, bool startTimeIsSet) {
        this->url = url;
        this->priority = priority;
        this->startDownloadTime = plannedTime;
        this->startTimeIsSet = startTimeIsSet;
    };
    time_t getStartDownloadTime() const { return this->startDownloadTime; }

    bool getStartTimeStatus() const { return this->startTimeIsSet; };

    bool isReadyToDownload() const { return this->startDownloadTime < time(NULL); };

    int getPriority() const { return this->priority; };

    string getUrl() const {
        return this->url;
    }

    string toString() const {
        stringstream result;
        if(this->startTimeIsSet) {
            struct tm tstruct{};
            char buf[80];
            tstruct = *localtime(&this->startDownloadTime);
            strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
            result << "Panned on: " <<    string(buf);
        } else {
            result << "Priority: " << this->priority;
        }
        result << "\t" << "URL: " << this->url;
        result << endl;
        return result.str();
    }
private:
};

inline bool operator< (const DownloadHandler& handler1, const DownloadHandler& handler2) {          //porovnava "priorutu"
    if (handler1.getStartTimeStatus() || handler2.getStartTimeStatus()) {
        time_t currTime = time(0);
        return handler1.getStartDownloadTime() - currTime > handler2.getStartDownloadTime() - currTime;       //Ak je nadstaveny cas stahovania od pouzivatela vyberie sa ten co je skor
    }

    return handler1.getPriority() > handler2.getPriority();                             //Inak sa vyberie podla priority
}
#endif
