COMP = g++
COMPFLAGS = -std=c++14

LDLIBS = -lpthread -lcurl

all: downloadManager
downloadManager: main.o
	$(COMP) $(COMPFLAGS) -o $@ $^ $(LDLIBS)

%.o: %.cpp
	$(COMP) $(COMPFLAGS) -c -o $@ $<



Logger.o: Logger.cpp
FIleManager.o: FIleManger.cpp
DownloadHandler.o: DownloadHandler.cpp

DownloadClient.o: clients/DownloadClient.cpp Logger.cpp
FtpClient.o: clients/FtpClient.cpp clients/UrlParser.cpp Logger.cpp clients/DownloadClient.cpp
FtpsClient.o: clients/FtpsClient.cpp clients/UrlParser.cpp Logger.cpp clients/DownloadClient.cpp
HttpClient.o: clients/HttpClient.cpp clients/UrlParser.cpp Logger.cpp clients/DownloadClient.cpp
UrlParser.o: clients/UrlParser.cpp Logger.cpp clients/DownloadClient.cpp

DownloadManager.o: DownloadManager.cpp DownloadHandler.cpp clients/HttpClient.cpp clients/FtpClient.cpp clients/FtpsClient.cpp clients/BitTorrentClient.cpp
main.o: DownloadManager.cpp main.cpp

BitTorrentClient.o: clients/BitTorrentClient.h