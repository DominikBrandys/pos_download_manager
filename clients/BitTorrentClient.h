//
// Created by samuel on 26.12.2022.
//
#include "DownloadClient.h"
#include "../BitTorrent/PeerRetriever.h"
#include "../BitTorrent/ParseTorrent.h"
#include "../BitTorrent/PieceManager.h"
#include "../BitTorrent/PeerConnection.h"
#include "../BitTorrent/SharedQueue.h"

#ifndef SEMESTRALKA_TORENTCLIENT_H
#define SEMESTRALKA_TORENTCLIENT_H

using namespace std;

class BitTorrentClient: private DownloadClient {
private:
    vector<PeerConnection*> connections;
    vector<thread> threadPool;
    int threadNum;
    string CLIENT_ID;
    SharedQueue<Peer*> queue;

    shared_ptr<Logger> logger_;
public:
    explicit BitTorrentClient(shared_ptr<Logger> logger) : DownloadClient(logger) {};
    string selectTorrentFile();
    ~BitTorrentClient();
    bool download(const string& selectedFile);
    void terminate();

};


#endif //SEMESTRALKA_TORENTCLIENT_H
