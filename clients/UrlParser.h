//
// Created by brand on 30. 12. 2022.
//
#include <curl/curl.h>
#include <curl/easy.h>
#include <arpa/inet.h>
#include <unistd.h>

#ifndef SEMESTRALKA_URLPARSER_H
#define SEMESTRALKA_URLPARSER_H

#include "string"
#include <iostream>

using namespace std;

// navod: https://curl.se/libcurl/c/libcurl-url.html
class UrlParser {
private:
    CURLU *handle; //drzi info z url
    // jenotlive casti... nie vzdy existuju vsetky..
    char *scheme;
    char *host;
    char *user;
    char *password;
    char *path;
    char *query;
    char *fragment;
    char *port;
    char * rawUrl;

    string homeDirectory;
public:
    ~UrlParser();
    explicit UrlParser(const char* rawUrl);
    char * getHost(); //facebook.com
    char * getScheme(); // http, https, ftp ....
    char * getUser();
    char * getPassword();
    char * getPath();
    char * getQuery();
    char * getFragment();
    char * getFullUrl();
    int getPort();
    void printAllParts();
    inline CURLUcode getPart(CURLUPart what, char ** part, unsigned int flags = 0);

    //IP getters
    string getSingleIP();
    string getFileNameFromPath();

private:
    bool fileExists(const string &filename);
};


#endif //SEMESTRALKA_URLPARSER_H
