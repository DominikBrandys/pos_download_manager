//
// Created by samuel on 26.12.2022.
//

#include "HttpClient.h"

HttpClient::~HttpClient() = default;

void HttpClient::download(const string& url) {

    UrlParser urlParser(url.c_str());
    string protocol(urlParser.getScheme());
    string fileName = urlParser.getFileNameFromPath();

    if(protocol == "https") {  //HTTPS
        this->httpsGet(&urlParser);
    } else {                                //HTTP
        stringstream request;
        request << "GET " << urlParser.getPath() << " HTTP/1.1\r\n";
        request << "Host: " << urlParser.getHost() << "\r\n\r\n";
        httpGet(request.str(), urlParser.getSingleIP(), urlParser.getPort(), fileName);
    }

    this->logHistory(url, fileName, "OK");
}

int HttpClient::connectSocket(string ip_address, int port) {
    int err = -1, socket_descriptor = -1;
    struct sockaddr_in socketAddress;

    memset(&socketAddress, '\0', sizeof(socketAddress));
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_addr.s_addr = inet_addr(ip_address.c_str());   // Server IP
    socketAddress.sin_port = htons(port);   // Server Port number
    socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_descriptor > 0)
    {
        err = connect(socket_descriptor, (struct sockaddr*) &socketAddress, sizeof(socketAddress));
    }
    if (err != -1) //success
    {
        return socket_descriptor;
    }
    return -1;
}

string HttpClient::getHeaderField(const string &fullHeader, const string &fieldName) {
    size_t pos = fullHeader.find(fieldName);

    string composedHeader;
    if (pos != string::npos)
    {
        size_t begin = fullHeader.find_first_not_of(": ", pos + fieldName.length());
        size_t until = fullHeader.find_first_of("\r\n\t ", begin + 1);
        if (begin != string::npos && until != string::npos)
        {
            composedHeader = fullHeader.substr(begin, until - begin);
        }
    }
    return composedHeader;
}

int HttpClient::httpGet(const string& request, const string& ip_address, int port, const string& localFileName) {
    stringstream header;
    char delimiter[] = "\r\n\r\n";
    char buffer[8000];
    int socket;
    int bytesReceived = -1;
    int bytesSofar = 0;
    int bytesExpected = -1;

    size_t bodyStart = 0;

    bool headerParsed = false;

    ofstream localFile(localFileName.c_str());
    if (localFile.good() && (socket = connectSocket(ip_address, port)) ) {
        ::send(socket, request.c_str(), request.length(), 0);
        while (bytesSofar != bytesExpected && (bytesReceived = ::recv(socket, buffer, sizeof(buffer), 0)) > 0) {
            if (!headerParsed && bytesReceived > 0) {   //parse header, take size and save rest of data
                string rawResponse(buffer);
                bodyStart = rawResponse.find(delimiter);

                if(bodyStart == string::npos) {
                    throw std::runtime_error("Invalid HTTP response: no body found");
                }

                bodyStart += 4;  //skip delimiter
                bytesExpected = stoi(getHeaderField(rawResponse.substr(0,bodyStart),"Content-Length"));

                localFile.write(&buffer[bodyStart],bytesReceived - bodyStart);      //save first part of body
                bytesSofar += bytesReceived - bodyStart;

                headerParsed = true;
            } else {
                //read body
                bytesSofar += bytesReceived;
                localFile.write(buffer, bytesReceived);
            }
        }
        ::close(socket);
        localFile.close();
    }
    return bytesSofar;
}

int HttpClient::httpsGet(UrlParser* urlParser) {
    CURL *curl;
    CURLcode res;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, urlParser->getFullUrl());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

        saveToFile(curl, urlParser);
        // perform request with return code
        res = curl_easy_perform(curl);
        if(res != CURLE_OK) {
            fprintf(stderr, "curl perform failed: %s\n",curl_easy_strerror(res));
        }
        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();

    return 0;
}

size_t HttpClient::writeToFile(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realSize = size * nmemb;
    auto file = reinterpret_cast<ofstream*>(userp);
    file->write(reinterpret_cast<const char*>(contents), realSize);
    return realSize;
}


void HttpClient::saveToFile(CURL *curl, UrlParser* urlParser) {
    static ofstream file(urlParser->getFileNameFromPath(), ios::binary);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeToFile);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, reinterpret_cast<void*>(&file));
}
