//
// Created by samuel on 26.12.2022.
//

#include <iostream>
#include <fstream>
#include <mutex>
#include <condition_variable>
#include <curl/curl.h>

#include "../Logger.h"

#ifndef SEMESTRALKA_DOWNLOADCLIENT_H
#define SEMESTRALKA_DOWNLOADCLIENT_H

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

using namespace std;

class DownloadClient {
public:
    explicit DownloadClient(shared_ptr<Logger> logger);

    ~DownloadClient();

    void displayProgress(double progress);

    long getRemoteFileSize(const string &url);

    void logHistory(const string &url, const string &fileName, const string &status);

private:
    shared_ptr<Logger> logger_;
};


#endif //SEMESTRALKA_DOWNLOADCLIENT_H
