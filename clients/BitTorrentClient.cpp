//
// Created by samuel on 26.12.2022.
//

#include "BitTorrentClient.h"
#include <iostream>
#include <thread>
#include <random>
#include <filesystem>
#include <vector>
#include <algorithm>


namespace fs = std::filesystem;

#define PEER_QUERY_INTERVAL 60 // 1 minute


BitTorrentClient::~BitTorrentClient() = default;


bool BitTorrentClient::download(const string& selectedFile) {
    this->threadNum = 4;

    //string selected_file = selectTorrentFile();
    string selected_file = selectedFile;
    if (selected_file.empty()) {
        std::cout << "Cancelled" << std::endl;
        return false;
    }


    string random_numbers = "POSDM_";
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0, 9);

    for (int i = 0; i < 14; i++) {
        random_numbers += to_string(dis(gen));
    }
    this->CLIENT_ID = random_numbers;

    ParseTorrent parseTorrent(CLIENT_ID);
    string url = parseTorrent.parse(selected_file);
    Torrent torrent = parseTorrent.getTorrent();
    if (torrent.InfoHash.empty()) {
        throw runtime_error("Torrent was not read correctly.");
    }

//    fs::path source_dir = CMAKE_CURRENT_SOURCE_DIR;

    // Construct the path to the Torrent directory
    string source_dir = ::getenv("HOME");
    source_dir += "/";
    fs::path downloadDirectory = source_dir;
    string filename = torrent.Name;
    string downloadPath = downloadDirectory.string() + filename;


    PieceManager* pieceManager = new PieceManager(torrent.PieceHashes, torrent.PieceLength, torrent.Length, threadNum, downloadPath);

    // Adds threads to the thread pool
    for (int i = 0; i < threadNum; i++)
    {
        PeerConnection connection(CLIENT_ID, torrent.InfoHash, pieceManager, &queue);
        connections.push_back(&connection);
        thread thread(&PeerConnection::start, connection);
        threadPool.push_back(move(thread));
    }

    auto lastPeerQuery = (time_t) (-1);

    //cout << "Download initiated..." << endl;

    while (true)
    {
        if (pieceManager->isComplete())
            break;

        time_t currentTime = time(nullptr);
        auto diff = difftime(currentTime, lastPeerQuery);
        // Retrieve peers from the tracker after a certain time interval or whenever
        // the queue is empty
        if (lastPeerQuery == -1 || diff >= PEER_QUERY_INTERVAL || queue.empty())
        {
            PeerRetriever* peerRetriever = new PeerRetriever(url, torrent);
            vector<Peer*> peers;
            try {
                peers = peerRetriever->retrievePeers(pieceManager->bytesDownloaded());
            } catch (runtime_error e) {
                cout << "Error getting response from tracker" << endl;
                return false;
            }
            lastPeerQuery = currentTime;
            if (!peers.empty())
            {
                queue.clear();
                for (auto peer : peers)
                    queue.push_back(peer);

            }
        }
    }

    terminate();

    if (pieceManager->isComplete())
    {
        //cout << "Download completed!" << endl;
        //cout << "File downloaded to " << downloadPath << endl;
        logHistory(selectedFile, torrent.Name, "OK");
        return true;
    }
    return false;
}



void BitTorrentClient::terminate()
{
    // Pushes dummy Peers into the queue so that
    // the waiting threads can terminate
    for (int i = 0; i < threadNum; i++)
    {
        Peer* dummyPeer = new Peer { "0.0.0.0", 0 };
        queue.push_back(dummyPeer);
    }
    for (auto connection : connections)
        connection->stop();

    for (thread& thread : threadPool)
    {
        if (thread.joinable())
            thread.join();
    }

    threadPool.clear();
}

string BitTorrentClient::selectTorrentFile() {
    // Set the path to the Torrent folder
    // Get the path of the current source directory
    fs::path source_dir = CMAKE_CURRENT_SOURCE_DIR;

    // Construct the path to the Torrent directory
    fs::path torrent_folder = source_dir / "Torrent";

    // Create a vector to store the file paths of the torrent files
    std::vector<fs::path> torrent_files;

    // Iterate over all the files in the Torrent folder
    for (const auto &entry : fs::directory_iterator(torrent_folder)) {
        // Check if the file has the ".torrent" extension
        if (entry.path().extension() == ".torrent") {
            // Add the file path to the vector
            torrent_files.push_back(entry.path());
        }
    }
    cout << "Torrenty sa nachadzaju v " << fs::absolute(torrent_folder) << endl;
    // Print the list of torrent files
    for (size_t i = 0; i < torrent_files.size(); i++) {
        std::cout << i + 1 << ": " << torrent_files[i].filename() << std::endl;
    }
    std::cout << "0: Koniec" << std::endl;

    // Set the maximum number of torrent files
    int max_torrents = torrent_files.size();

    int choice;
    do {
        // Print a message asking the user to choose a number
        std::cout << "Vyber Torrent na stiahnutie cislom (0 pre koniec): ";
        std::cin >> choice;
    } while (choice < 0 || choice > max_torrents);
    // If the user did not choose 0 (cancel), return the absolute file path of the selected torrent file
    if (choice > 0) {
        cout << fs::absolute(torrent_files[choice - 1]) << endl;
        return fs::absolute(torrent_files[choice - 1]);
    }

    // Return a null path if the user chose 0 (cancel)
    return "";
}