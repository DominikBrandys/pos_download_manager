//
// Created by samuel on 26.12.2022.
//

#include <cstring>
#include "DownloadClient.h"

static size_t throwAway(void *ptr, size_t size, size_t nmemb, void *data) {
    (void) ptr;
    (void) data;
    return (size_t) (size * nmemb);
}

DownloadClient::DownloadClient(shared_ptr<Logger> logger) {
    this->logger_ = logger;
}

void DownloadClient::logHistory(const string& url, const string& fileName, const string& status) {
    string logMessage = status + "\t\t" + fileName  + "\t\t\t" + url;
    this->logger_->writeMessage(logMessage);
}

void DownloadClient::displayProgress(double progress) {
    int percentage = (int) (progress * 100);
    int lpad = (int) (progress * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf("\r%3d%% [%.*s%*s]", percentage, lpad, PBSTR, rpad, "");
    fflush(stdout);
}

/**
* Returns size of remote file.
* Source: https://curl.se/libcurl/c/ftpgetinfo.html
*/
long DownloadClient::getRemoteFileSize(const string &url) {
    CURL *curl;
    CURLcode res;
    curl_off_t fileSize = 0;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
        curl_easy_setopt(curl, CURLOPT_FILETIME, 1L);
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, throwAway);
        curl_easy_setopt(curl, CURLOPT_HEADER, 0L);

        res = curl_easy_perform(curl);

        if (CURLE_OK == res) {
            /* https://curl.se/libcurl/c/curl_easy_getinfo.html */
            curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &fileSize);
        } else {
            fprintf(stderr, "curl told us %d\n", res);
        }
        curl_easy_cleanup(curl);
    }

    curl_global_cleanup();

    return fileSize;
}

DownloadClient::~DownloadClient() = default;
