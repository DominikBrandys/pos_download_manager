//
// Created by samuel on 26.12.2022.
//

#include "DownloadClient.h"

#ifndef SEMESTRALKA_HTTPCLIENT_H
#define SEMESTRALKA_HTTPCLIENT_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <fstream>
#include <string>
#include <iostream>
#include <iosfwd>
#include <unistd.h>
#include <memory.h>
#include "HttpClient.h"
//HTTPS
#include <curl/curl.h>
#include <curl/easy.h>

#include "../Logger.h"

#include <netinet/in.h>
#include <sstream>

#include "UrlParser.h"

using namespace std;

class HttpClient : public DownloadClient {
public:
    HttpClient(shared_ptr<Logger> logger) : DownloadClient(logger) {};

    ~HttpClient();

    void download(const string &url);

    int connectSocket(string ip_address, int port);

    string getHeaderField(const string &fullHeader, const string &fieldName);

    int httpGet(const string &request, const string &ip_address, int port, const string &localFileName);

    int httpsGet(UrlParser *urlParser);

private:
    static size_t writeToFile(void *contents, size_t size, size_t nmemb, void *userp);

    void saveToFile(CURL *curl, UrlParser *urlParser);
};


#endif //SEMESTRALKA_HTTPCLIENT_H
