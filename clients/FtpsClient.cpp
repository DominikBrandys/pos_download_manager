//
// Created by samuel on 2.1.2023.
//

#include "FtpsClient.h"

struct FtpFile {
    const char *filename;
    FILE *stream;
};

static size_t writeToFile(void *buffer, size_t size, size_t nmemb, void *stream) {
    struct FtpFile *out = (struct FtpFile *) stream;
    if (!out->stream) {
        out->stream = fopen(out->filename, "wb");
        if (!out->stream)
            return -1;
    }
    return fwrite(buffer, size, nmemb, out->stream);
}

FtpsClient::~FtpsClient() = default;

/**
 * Source: https://curl.se/libcurl/c/ftpget.html
 */
bool FtpsClient::download(const string &url) {
    UrlParser *urlParser = new UrlParser(url.c_str());
    CURL *curl;
    CURLcode res;
    string fileName = urlParser->getFileNameFromPath();
    struct FtpFile ftpFileData = {};
    ftpFileData.filename = fileName.c_str();
    ftpFileData.stream = nullptr;

    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FTP_USE_EPRT, true);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeToFile);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpFileData);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, false);  // for debug true

        res = curl_easy_perform(curl);

        curl_easy_cleanup(curl);

        if (CURLE_OK != res) {
            if (Logger::DEBUG) fprintf(stderr, "curl told us %d\n", res);
            return false;
        }
    }

    if (ftpFileData.stream) {
        fclose(ftpFileData.stream);
    }

    curl_global_cleanup();
    delete urlParser;
    this->logHistory(url, fileName, "OK");
    return true;
}
