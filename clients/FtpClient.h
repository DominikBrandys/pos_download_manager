//
// Created by samuel on 26.12.2022.
//

#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstring>
#include <unistd.h>
#include <netdb.h>    //hostent\
#include <cstdio>
#include "DownloadClient.h"
#include "UrlParser.h"

using namespace std;

#ifndef SEMESTRALKA_FTPCLIENT_H
#define SEMESTRALKA_FTPCLIENT_H

using namespace std;

const int SIZE = 1024;          // Size of Buffers

class FtpClient : private DownloadClient {
public:
    explicit FtpClient(shared_ptr<Logger> logger) : DownloadClient(logger) {};

    ~FtpClient();

    bool download(const string &url);     // returns file path

private:
    int createAndConnectToSocket(int &socketOrg, const char *serverIp, unsigned short serverPort);

    void sendCommand(int socket, string command, bool printToConsole);

    bool login(int socket, UrlParser *urlParser);

    void parsePasvResponse(const string &response, string &serverIp, unsigned short &serverPort);

    void receiveAll(int dataSocket, const char *fileName, const string &url);

    void printServerResponse();

private:
    char receiveBuff[SIZE];         // Buffer to receive to the server
    char sendBuff[SIZE];            // Buffer to send from server
    string contents;
};


#endif //SEMESTRALKA_FTPCLIENT_H
