//
// Created by brand on 30. 12. 2022.
//

#include <netdb.h>
#include <sstream>
#include "UrlParser.h"

UrlParser::UrlParser(const char * rawUrl) {
    this->homeDirectory = string(getenv("HOME"));
    this->handle = curl_url();
    curl_url_set(this->handle,CURLUPART_URL,rawUrl,0);

    curl_url_get(this->handle, CURLUPART_HOST, &host, 0);
    curl_url_get(this->handle, CURLUPART_SCHEME, &scheme, 0);
    curl_url_get(this->handle, CURLUPART_USER, &user, 0);
    curl_url_get(this->handle, CURLUPART_PASSWORD, &password, 0);
    curl_url_get(this->handle, CURLUPART_PORT, &port, CURLU_DEFAULT_PORT);
    curl_url_get(this->handle, CURLUPART_PATH, &path, 0);
    curl_url_get(this->handle, CURLUPART_QUERY, &query, 0);
    curl_url_get(this->handle, CURLUPART_FRAGMENT, &fragment, 0);
    curl_url_get(this->handle, CURLUPART_URL, &this->rawUrl, 0);
}

char * UrlParser::getHost() {
    return this->host;
}

CURLUcode UrlParser::getPart(CURLUPart what, char **part, unsigned int flags) {
    return curl_url_get(this->handle,what,part,flags);
}

UrlParser::~UrlParser() {
    curl_free(host);
    curl_free(port);
    curl_free(path);
    curl_free(query);
    curl_free(fragment);
    curl_free(password);
    curl_free(user);
    curl_free(scheme);
    curl_free(rawUrl);
    curl_url_cleanup(handle);
}

char *UrlParser::getScheme() {
    return this->scheme;
}

char *UrlParser::getFragment() {
    return this->fragment;
}

char *UrlParser::getUser() {
    return this->user;
}

char *UrlParser::getPassword() {
    return this->password;
}

char *UrlParser::getPath() {
    return this->path;
}

char *UrlParser::getQuery() {
    return this->query;
}

int UrlParser::getPort() {
    return atoi(this->port);
}

char * UrlParser::getFullUrl() {
    return this->rawUrl;
}

void UrlParser::printAllParts() {
    cout << "Schema: " << endl;
    printf(this->scheme);
    cout << endl << "Host: " << endl;
    printf(this->host);
    cout << endl << "Port:" << endl;
    printf(this->port);
    cout << endl << "Path:" << endl;
    printf(this->getPath());
}

/**
 * Returns IP address of HOST
 */
string UrlParser::getSingleIP() {
    struct hostent *host;
    host = gethostbyname(this->getHost());
    if (host == (struct hostent *)nullptr)
    {
        perror("Client: gethostbyname");
        return nullptr;
    }
    char* ip=inet_ntoa(*(struct in_addr*)host->h_addr_list[0]);
    return ip;
}

/**
 * Returns available suggested file name.
 */
string UrlParser::getFileNameFromPath() {
    string filePath = string(this->getPath());
    unsigned int index = filePath.find_last_of('/') + 1;
    unsigned int lengthOfName = filePath.length() - index;
    string fileName = filePath.substr(index, lengthOfName);
    string finalFileName = fileName;
    int counter = 1;
    while (fileExists(finalFileName)) {
        finalFileName = "(" + to_string(counter) + ") " + fileName;
        counter++;
    }

    return (this->homeDirectory + "/" + finalFileName);
}

/**
 * Checks if file exists in system
 */
bool UrlParser::fileExists(const string &filename) {
    string fullFilePath = this->homeDirectory + "/" + filename;
    return access(fullFilePath.c_str(), 0 ) == 0;
}