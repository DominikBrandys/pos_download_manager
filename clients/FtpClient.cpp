//
// Created by samuel on 26.12.2022.
//
#include "FtpClient.h"

FtpClient::~FtpClient() = default;

bool FtpClient::download(const string &url) {
    UrlParser *urlParser = new UrlParser(url.c_str());
    int ftpPort = urlParser->getPort();

    /* Create control socket to connect to FTP server */
    int controlSocket;
    if (this->createAndConnectToSocket(controlSocket, urlParser->getSingleIP().c_str(), (ftpPort != 0) ? ftpPort : 21) == 0) {
//        Logger::debugPrint("Something went wrong when getting file from " + string(urlParser->getPath()));
        return false;
    };

    /* Login to server */
    read(controlSocket, this->receiveBuff, sizeof(this->receiveBuff) - 1);
    this->printServerResponse();
    bool loggedIn = this->login(controlSocket, urlParser);
    if (!loggedIn) {
        close(controlSocket);
        return false;
    }

    /* Change mode to PASV */
    this->sendCommand(controlSocket, "PASV\r\n", true);
    string dataSocketIp;
    unsigned short dataSocketPort;
    this->parsePasvResponse(this->receiveBuff, dataSocketIp, dataSocketPort);
//    Logger::debugPrint("Creating data connection: " + dataSocketIp + " " + to_string(dataSocketPort));

    /* Create data socket for downloading */
    int dataSocket;
    if (this->createAndConnectToSocket(dataSocket, dataSocketIp.c_str(), dataSocketPort) == 0) {
//        Logger::debugPrint("Something went wrong when getting file from " + string(urlParser->getPath()));
        return false;
    };

    /* Send RETR on control socket to get file */
    string retrCommnad = "RETR " + string(urlParser->getPath()) + "\r\n";
    try {
        this->sendCommand(controlSocket, retrCommnad, true);
    } catch (...) {
//        Logger::debugPrint("Something went wrong when getting file from " + url);
        return false;
    }

    /* Save file (data socket) to local system */
//    Logger::debugPrint("Starting downloading file from url: " + url);
    string fileName = urlParser->getFileNameFromPath();
    receiveAll(dataSocket, fileName.c_str(), url);

    /* Close sockets */
    close(dataSocket);
    string quitCommand = "QUIT\r\n";
    this->sendCommand(controlSocket, quitCommand, true);
    close(controlSocket);

    delete urlParser;
    this->logHistory(url, fileName, "OK");
    return true;
}

int FtpClient::createAndConnectToSocket(int &socketOrg, const char *serverIp, const unsigned short serverPort) {
    struct sockaddr_in sockAddrData{};                                            // Internet address of server
    memset(&sockAddrData, 0, sizeof(sockAddrData));       // Clear structure
    sockAddrData.sin_family = AF_INET;       // Set address typedef
    sockAddrData.sin_addr.s_addr = inet_addr(serverIp);     // Set server IP address
    sockAddrData.sin_port = htons(serverPort);        // Use FTP port

    socketOrg = socket(AF_INET, SOCK_STREAM, 0);
    if (socketOrg < 0) {
//        Logger::debugPrint("Client: generate error");
        return 0;
    }

    if (connect(socketOrg, (struct sockaddr *) &sockAddrData, sizeof(sockAddrData)) < 0) {
  //      Logger::debugPrint("Client: connect error");
        return 0;
    }

    return socketOrg;
}

void FtpClient::sendCommand(int socket, const string command, bool printToConsole = false) {
    memset(this->receiveBuff, 0, SIZE);
    memset(this->sendBuff, 0, SIZE);
    strcpy(this->sendBuff, command.c_str());
    send(socket, this->sendBuff, strlen(this->sendBuff), 0);
    read(socket, this->receiveBuff, sizeof(this->receiveBuff) - 1);

    if (printToConsole) {
        this->printServerResponse();
    }
}

bool FtpClient::login(int socket, UrlParser *urlParser) {
    string user;
    try {
        user = string(urlParser->getUser());
    } catch (...) {
        user = "anonymous";
    }
    string loginCommand = "USER " + user + "\r\n";
    this->sendCommand(socket, loginCommand, false);

    string pass;
    try {
        pass = string(urlParser->getPassword());
    } catch (...) {
        pass = "anonymous";
    }
    string passCommand = "PASS " + pass + "\r\n";
    this->sendCommand(socket, passCommand, true);

    if (this->receiveBuff[0] != '2') {
        return false;
//        printf("Login was not successful, you need to enter credentials.\n");
//        printf("Please enter login: \n");
//        cin >> user;
//        loginCommand = "USER " + user + "\r\n";
//        this->sendCommand(socket, loginCommand, false);
//        printf("Please enter password: \n");
//        cin >> pass;
//        passCommand = "PASS " + pass + "\r\n";
//        this->sendCommand(socket, passCommand, true);
//        if (this->receiveBuff[0] != '2') {
//            printf("Login was not successful, incorrect credentials.\n");
//            return false;
//        }
//        return true;
    }

    return true;
}

/**
 * Function for parsing result of PASV command.
 * This functions sets parameters serverIp and serverPort from given response.
 * Response may look as: (44,241,66,173,4,4)
 *
 * https://github.com/pedro-vicente/lib_netsockets/blob/master/src/ftp.cc
 * https://cr.yp.to/ftp/retr.html
 */
void FtpClient::parsePasvResponse(const string &response, string &serverIp, unsigned short &serverPort) {
    unsigned int ipAdd[4];
    unsigned int port[2];
    char ip[100];

    size_t position = response.find('(');
    string strIp = response.substr(position + 1);
    sscanf(strIp.c_str(), "%u,%u,%u,%u,%u,%u", &ipAdd[0], &ipAdd[1], &ipAdd[2], &ipAdd[3], &port[0], &port[1]);
    serverPort = static_cast<unsigned short>(port[0] * 256 + port[1]);
    sprintf(ip, "%u.%u.%u.%u", ipAdd[0], ipAdd[1], ipAdd[2], ipAdd[3]);
    serverIp = ip;
}

/**
 * Writes data from socket to local file.
 *
 * https://github.com/pedro-vicente/lib_netsockets/blob/master/src/ftp.cc
 */
void FtpClient::receiveAll(int dataSocket, const char *fileName, const string &url) {
    long receiveSize;
    const int flags = 0;
    const int bufferSize = 4096;
    char buffer[bufferSize];
    FILE *file;

    long fileSize = Logger::DEBUG ? this->getRemoteFileSize(url) : 0;
    long downloaded = 0;

    file = fopen(fileName, "wb");
    while (true) {
        if ((receiveSize = recv(dataSocket, buffer, bufferSize, flags)) == -1) {
//           Logger::debugPrint("recv error: " + string(strerror(errno)));
        }

        downloaded += receiveSize;
        double progress = downloaded / (double) fileSize;
        if (Logger::DEBUG) {
            this->displayProgress(progress);
        }

        if (receiveSize == 0) {
//            Logger::debugPrint("File downloaded.");
            break;
        }

        fwrite(buffer, receiveSize, 1, file);
    }
    fclose(file);
}

void FtpClient::printServerResponse() {
    if (Logger::DEBUG) {
        printf("%s\n", this->receiveBuff);
    }
}
