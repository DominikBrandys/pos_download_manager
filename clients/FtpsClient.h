//
// Created by samuel on 2.1.2023.
//

#ifndef SEMESTRALKA_FTPSCLIENT_H
#define SEMESTRALKA_FTPSCLIENT_H

#include <sys/stat.h>
#include <cstring>
#include "DownloadClient.h"
#include "UrlParser.h"

using namespace std;

class FtpsClient : private DownloadClient {
public:
    explicit FtpsClient(shared_ptr<Logger> logger) : DownloadClient(logger) {};

    ~FtpsClient();

    bool download(const string &url);     // returns file path
};


#endif //SEMESTRALKA_FTPSCLIENT_H
