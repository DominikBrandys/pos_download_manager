//
// Created by brand on 4. 1. 2023.
//

#ifndef SEMESTRALKA_LOGGER_H
#define SEMESTRALKA_LOGGER_H


#include <string>
#include <fstream>
#include <ctime>
#include <mutex>
#include <iostream>

using namespace std;

class Logger {
public:
    explicit Logger(const string& fileName);

    void writeMessage(const string& message);

    void printContentOfFile();

    static void debugPrint(const string& message);

private:
    string getCurrentTime();
private:
    string fileName = "log.txt";
    mutex loggerMutex;
    ofstream outFile;

public:
    static const bool DEBUG = false;
};


#endif //SEMESTRALKA_LOGGER_H
