//
// Created by brand on 6. 1. 2023.
//

#ifndef SEMESTRALKA_FILEMANGER_H
#define SEMESTRALKA_FILEMANGER_H

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include <fstream>
#include <dirent.h>

class FIleManager {
public:
    void run() {
        while (true) {
            std::cout << "_________________" << std::endl;
            std::cout << "FILE MANAGER MENU" << std::endl;
            std::cout << "_________________" << std::endl;
            std::cout << "Zadajte prikaz (create, move, delete, print, exit): ";
            std::string command;
            std::cin >> command;
            std::cout << std::endl;
            if (command == "create") {
                createFile();
            } else if (command == "move") {
                moveFile();
            } else if (command == "delete") {
                deleteFile();
            } else if (command == "print") {
                printDirectory();
            } else if (command == "exit") {
                break;
            } else {
                std::cout << "Neplatny prikaz." << std::endl;
            }
        }
    }

    FIleManager() {
        this->dirPath = getenv("HOME");
        chdir(dirPath.c_str());
    }
    ~FIleManager() = default;
private:
    string dirPath;
    void createFile() {
        std::cout << "Vytvaranie noveho suboru" << std::endl;
        std::cout << "Zadajte nazov noveho suboru: ";
        std::string filename;
        std::cin >> filename;
        std:std::cout << std::endl;
        std::ofstream file(filename);
        file.close();
        std::cout << "Subor vytvoreny." << std::endl;
    }

    void moveFile() {
        std::cout << "Zadajte nazov suboru na presun: ";
        std::string filename;
        std::cin >> filename;

        std::cout << std::endl << "Zadajte cestu k novemu umiestneniu suboru (novacesta/) " << filename << " :";
        std::string new_name;
        std::cin >> new_name;
        new_name += filename;
        if (rename(filename.c_str(), new_name.c_str()) != 0) {
            std::cout << std::endl << "Chyba presunu suboru" << std::endl;
        } else {
            std::cout << std::endl << "Subor uspesne presunuty" << std::endl;
        }
    }

    void deleteFile() {
        std::cout << "Zadajte nazov suboru na vymazanie: ";
        std::string filename;
        std::cin >> filename;
        std::cout << endl << filename;
        int err;
        if ((err = remove(filename.c_str())) != 0) {
            std::cout << std::endl << "Chyba pri mazani suboru" << std::endl;
            std::cout << err << std::endl;
        } else {
            std::cout << std::endl << "Subor uspesne vymazany" << std::endl;
        }
    }

    void printDirectory() {
        std::string dirPath = getenv("HOME");
        std::cout << "Obsah priecunku " << dirPath << ": " << std::endl;
        DIR* dir;
        struct dirent* ent;
        if ((dir = opendir(dirPath.c_str())) != NULL) {
            // print content
            while ((ent = readdir(dir)) != NULL) {
                printf("%s\n", ent->d_name);
            }
            closedir(dir);
        } else {
            perror("");
            return;
        }
    }
};


#endif //SEMESTRALKA_FILEMANGER_H
