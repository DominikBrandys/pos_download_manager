//
// Created by brand on 25. 12. 2022.
//

#include "DownloadManager.h"


DownloadManager::DownloadManager() {
    this->downloadQueue = new priority_queue<DownloadHandler>();
    this->plannedDownloadQueue = new priority_queue<DownloadHandler>();
    this->activeThreadsContainer = new vector<thread>();
    this->fileManager = new FIleManager();
    string composedPath = getenv("HOME");

    historyLogFilePath = composedPath + "/downloadHistory.txt";
    this->logger_ = make_shared<Logger>(historyLogFilePath);
}

DownloadManager::~DownloadManager() {
    delete this->downloadQueue;
    delete this->activeThreadsContainer;
    delete this->plannedDownloadQueue;
    delete this->fileManager;
}

void DownloadManager::startManager() {

    cout << "Vitajte v download manager POS" << endl;
    cout << "------------------------------" << endl;
    int volbaHlavneMenu;
    string vybranaUrl;
    thread downloadLoop2 = thread(&DownloadManager::startDownloadingLoop, this);
    do {


        cout << "Moznosti: " << endl;
        cout << "[1] Pridanie stahovania podla priority" << endl;
        cout << "[2] Pridanie stahovania s naplanovanym casom" << endl;
        cout << "[3] Stahovanie torrentov" << endl;
        cout << "[4] Historia" << endl;
        cout << "[5] Aktualna fronta stahovania" << endl;
        cout << "[6] Spravovat subory" << endl;
        cout << "[0] Koniec" << endl;

        cout << "Volba: ";
        cin >> volbaHlavneMenu;
        cout << endl;

        if (volbaHlavneMenu == 0) { //Koniec programu
            cout << "KONIEC" << endl;
            this->downloadLoopMutex.lock();
            this->downloadLoopRun = false; //TODO MUTEX
            this->downloadLoopMutex.unlock();
            break;
        } else if (volbaHlavneMenu == 1) {
            vybranaUrl = this->offerUrl();
            int priorita;
            cout << "Zadajte prioritu: ";
            cin >> priorita;
            cout << endl;
            this->downloadQueue->push(DownloadHandler(vybranaUrl, priorita, time(NULL), false));      //stahovanie bez nadstavenia casu
            cout << "Subor na stiahnutie pridany, priorita: " << priorita << ", URL: " << vybranaUrl << endl;
            cout << "Celkovy pocet poloziek na stiahnutie : " << this->downloadQueue->size() + this->plannedDownloadQueue->size() << endl << endl;
        } else if (volbaHlavneMenu == 2) {
            vybranaUrl = this->offerUrl();
            time_t startTime = this->createTime();
            this->plannedDownloadQueue->push(DownloadHandler(vybranaUrl, 1, startTime, true));      //stahovanie s naplanovanim casu
            cout << "Subor na stiahnutie pridany URL: " << vybranaUrl << endl;
            cout << "Celkovy pocet poloziek na stiahnutie : " << this->downloadQueue->size()+ this->plannedDownloadQueue->size() << endl << endl;
        } else if (volbaHlavneMenu == 3) {
            try {
                int prioritaTorrent;
                BitTorrentClient torrentClient(this->logger_);
                string torrentFile = torrentClient.selectTorrentFile();
                cout << "Zadajte prioritu: ";
                cin >> prioritaTorrent;
                cout << endl;
                this->plannedDownloadQueue->push(DownloadHandler(torrentFile, prioritaTorrent, time(NULL), false));
                cout << "Stahujem torrent: " << torrentFile << endl;
                cout << "Celkovy pocet poloziek na stiahnutie : " << this->downloadQueue->size()+ this->plannedDownloadQueue->size() << endl << endl;
            } catch (runtime_error e) {
                cout << "error: " << e.what() << endl;
            }
        } else if (volbaHlavneMenu == 4) {
            this->logger_->printContentOfFile();
        } else if (volbaHlavneMenu == 5) {
            cout << "Akrualna polozka v poradi: " << endl;
            cout << "---------------------------" << endl;
            if (!this->downloadQueue->empty()) {
                cout << this->downloadQueue->top().toString();
            } else {
                cout << "Ziadna polozka v prioritnom poradi." << endl;
            }
            cout << "---------------------------" << endl;
            if (!this->plannedDownloadQueue->empty()) {
                cout << this->plannedDownloadQueue->top().toString();
            } else {
                cout << "Ziadna polozka cakajuca na planovane nacasovane stiahnutie.";
            }
            cout << "---------------------------" << endl;
        }
        if(volbaHlavneMenu == 6) {
            cout << "Spustam FILE MANAGER" << endl;
            cout << "____________________" << endl;
            this->fileManager->run();
        }
    } while (true);
    downloadLoop2.join();
}

string DownloadManager::offerUrl() {
    int volba;
    bool volbaPlatna = false;
     do {
        cout << "Vyberte URL zdroju" << endl;
        for (int i = 0; i < LENGTH_PREPARED_URLS; i++) {
            cout << "[" << i << "] " << this->preparedUrls[i] << endl;
        }
        cout << "[7] Zadajte vlastnu URL" << endl;
        cout << "Volba: ";
        cin >> volba;
        cout << endl;
        if (volba < 0 || volba > LENGTH_PREPARED_URLS + 1) {
            cout << "Neplatna volba, skuste znovu." << endl;
            volbaPlatna = false;
        } else {
            volbaPlatna = true;
        }
        if (volba == LENGTH_PREPARED_URLS + 1) {
            string vlastnaURL;
            cout << "Zadajte URL: ";
            cin >> vlastnaURL;
            return vlastnaURL;
        }

    } while(!volbaPlatna);
    return this->preparedUrls[volba];
}

void DownloadManager::download(const string &url) {
    switch (this->chooseProtocol(url)) {
        case HTTP_PROTOCOL:
            this->httpDownload(url);
            break;
        case FTP_PROTOCOL:
            this->ftpDownload(url);
            break;
        case FTPS_PROTOCOL:
            this->ftpsDownload(url);
            break;
        case BIT_TORRENT_PROTOCOL:;
            this->torrentDownload(url);
            break;
        default:
            this->torrentDownload(url);
            break;
    }
    this->activeThreads--;
}

int DownloadManager::chooseProtocol(const string &url) {
    //ak protocol konci na ".torrent" je to torrent
    string suffix = ".torrent";
    if (url.compare(url.length() - suffix.length(), suffix.length(), suffix) == 0) {
        return BIT_TORRENT_PROTOCOL;
    }
    UrlParser urlParser(url.c_str());

    string protocol = "";
    try {
        protocol = string((urlParser.getScheme()));
    } catch (...) {
        return BIT_TORRENT_PROTOCOL;
    }

    if (protocol == "http" || protocol == "https") {        //TODO overit s bittorentom, ak tak to pojde osobitne. Mozno URL parser bude vediet idk..
        return HTTP_PROTOCOL;
    } else if (protocol == "ftp") {
        return FTP_PROTOCOL;
    } else if (protocol == "ftps") {
        return FTPS_PROTOCOL;
    } else {
        return BIT_TORRENT_PROTOCOL;
    }
}

void DownloadManager::startDownloadingLoop() {
    do {
        downloadLoopMutex.unlock();
        this->downloadQueueMutex.lock();
        if (this->activeThreads < MAX_ACTIVE_DOWNLOAD_THREADS && this->downloadQueue->size() > 0) {
            string url = this->downloadQueue->top().getUrl();
            this->downloadQueue->pop();
            this->activeThreadsContainer->push_back(thread(&DownloadManager::download, this, url));
            this->activeThreads++;
        }
        this->downloadQueueMutex.unlock();
        sleep(1);   //sleep for delay...

        if(!this->plannedDownloadQueue->empty() && this->plannedDownloadQueue->top().isReadyToDownload()) {
            string plannedUrl = this->plannedDownloadQueue->top().getUrl();
            this->plannedDownloadQueue->pop();
            this->activeThreadsContainer->push_back(thread(&DownloadManager::download,this,plannedUrl));
        }

        this->downloadLoopMutex.lock();
    } while (downloadLoopRun);
    this->activeThreadsMutex.lock();
    for (auto &thread: *this->activeThreadsContainer) {
        thread.join();
    }
    this->activeThreadsMutex.unlock();
}

void DownloadManager::httpDownload(const string &url) {
    HttpClient httpClient(this->logger_);
    try{
        httpClient.download(url);
    } catch (...) {
        string logMessage = "FAILED\t" + url;
        this->loggerMutex.lock();
        this->logger_->writeMessage(logMessage);
        this->loggerMutex.unlock();
    }
}

void DownloadManager::ftpDownload(const string &url) {
    FtpClient ftpClient(this->logger_);
    bool result = ftpClient.download(url);
    if (!result) {
        string logMessage = "FAILED\t" + url;
        this->loggerMutex.lock();
        this->logger_->writeMessage(logMessage);
        this->loggerMutex.unlock();
    }
}

void DownloadManager::ftpsDownload(const string &url) {
    FtpsClient ftpsClient(this->logger_);
    bool result = ftpsClient.download(url);
    if (!result) {
        string logMessage = "FAILED\t" + url;
        this->loggerMutex.lock();
        this->logger_->writeMessage(logMessage);
        this->loggerMutex.unlock();
    }
}



void DownloadManager::torrentDownload(const string &url) {
    BitTorrentClient torrentClient(this->logger_);
    bool result = torrentClient.download(url);

    if (!result) {
        string logMessage = "FAILED\t" + url;
        this->loggerMutex.lock();
        this->logger_->writeMessage(logMessage);
        this->loggerMutex.unlock();
    }
}

time_t DownloadManager::createTime() {
    cout << "Naplanovanie casu stahovania. Zadajte cas v poradi: MM(mesiac) DD HH MM(min)" << endl;
    int month, day, hour, minutes;
    cout << "Mesiac: ";
    cin >> month;
    cout << endl;
    cout << "Den: ";
    cin >> day;
    cout << endl;
    cout << "Hodina: ";
    cin >> hour;
    cout << endl;
    cout << "Minuta: ";
    cin >> minutes;
    cout << endl;

    time_t now = time(0);
    tm *ltm = localtime(&now);

    tm tm{};  // zero initialise
    tm.tm_year = ltm->tm_year; // 2020
    tm.tm_mon = month - 1; // February
    tm.tm_mday = day; // 15th
    tm.tm_hour = hour;
    tm.tm_min = minutes;
    tm.tm_isdst = 0; // Not daylight saving
    time_t t = mktime(&tm);
    char *dt = ctime(&t);
    cout << "naplanovany cas: " << dt << endl;
    return t;
}
