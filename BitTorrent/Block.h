//
// Created by Patrik on 02.01.2023.
//

#ifndef SEMESTRALKA_BLOCK_H
#define SEMESTRALKA_BLOCK_H

#include <istream>

using namespace std;

enum BlockStatus
{
    missing = 0,
    pending = 1,
    retrieved = 2
};

struct Block
{
    int piece;
    int offset;
    int length;
    BlockStatus status;
    string data;
};

#endif //SEMESTRALKA_BLOCK_H
