//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#ifndef SEMESTRALKA_PIECE_H
#define SEMESTRALKA_PIECE_H

#include <vector>
#include "Block.h"

class Piece {
private:
    string hashValue;
public:
    int index;
    vector<Block*> blocks;
    Piece(int index, vector<Block*> blocks, string hashValue);
    ~Piece();
    string getData();
    Block* nextRequest();
    void blockReceived(int offset, string data);
    bool isComplete();
    bool isHashMatching();
    void reset();
};


#endif //SEMESTRALKA_PIECE_H
