//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#ifndef BITTORRENTCLIENT_CONNECT_H
#define BITTORRENTCLIENT_CONNECT_H
#include <string>

using namespace std;

int createConnection(const string& ip, int port);
void sendData(int sock, const string& data);
string receiveData(int sock, uint32_t bufferSize = 0);

#endif //BITTORRENTCLIENT_CONNECT_H
