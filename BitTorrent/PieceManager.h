//
// Created by Patrik on 02.01.2023.
//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#ifndef SEMESTRALKA_PIECEMANAGER_H
#define SEMESTRALKA_PIECEMANAGER_H

#include "Piece.h"
#include <vector>
#include <map>
#include <ctime>
#include <mutex>
#include <fstream>
#include <thread>

#include "Piece.h"
#include "ParseTorrent.h"

struct PendingRequest
{
    Block* block;
    time_t timestamp;
};

class PieceManager {
private:
    map<string, string> peers;
    vector<Piece*> missingPieces;
    vector<Piece*> ongoingPieces;
    vector<Piece*> completedPieces;
    long pieceLength;
    int maximumConnections;
    long fileLength;
    unsigned long totalPieces;
    mutex lock;
    bool test = false;
    bool hasPiece(const string& bitField, int index);
    void setPiece(string& bitField, int index);
    void write(Piece* piece);
    ofstream downloadedFile;
    vector<PendingRequest*> pendingRequests;
public:

    PieceManager(vector<string> pieces, string pieceLength, string fileLength, int threadNum, string downloadPath);
    ~PieceManager();
    bool isComplete();

    Block* expiredRequest(string peerId);
    Block* nextOngoing(string peerId);
    Piece* getRarestPiece(string peerId);

    unsigned long bytesDownloaded();
    Block* nextRequest(string peerId);
    vector<Piece *> processPieces(vector<string> pieces);
    bool test_f();
    void addPeer(const string& peerId, string bitField);
    void removePeer(const string& peerId);
    void updatePeer(const string& peerId, int index);

    void blockReceived(string basicString, int i, int i1, string basicString1);

    Block *nextPendingBlock(string basicString);

    Piece *getNextPiece(string basicString);
};


#endif //SEMESTRALKA_PIECEMANAGER_H
