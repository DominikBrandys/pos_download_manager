//
// Created by Patrik on 02.01.2023.
//
// credit: https://github.com/ss16118/torrent-client-cpp
//
#include <stdexcept>
#include <iostream>
#include <cassert>
#include <cstring>
#include <chrono>
#include <unistd.h>
#include <netinet/in.h>
#include <utility>

#include "PeerConnection.h"
#include "message.h"


#define INFO_HASH_STARTING_POS 28
#define PEER_ID_STARTING_POS 48
#define HASH_LEN 20
#define DUMMY_PEER_IP "0.0.0.0"


PeerConnection::~PeerConnection() {
    closeSock();
}

// the downloading process
void PeerConnection::start() {
    //cout << "Downloading thread started..." << endl;

    while (!(terminated || pieceManager->isComplete()))
    {
        peer = queue->pop_front();
        // Terminates the thread if it has received a dummy Peer
        if (peer->ip == DUMMY_PEER_IP)
            return;

        try
        {
            // Establishes connection with the peer, and lets it know
            // that we are interested.
            if (establishNewConnection())
            {
                while (!pieceManager->isComplete())
                {
                    message message = receiveMessage();
                    if (message.getMessageId() > 10)
                        throw runtime_error("Received invalid message Id from peer " + peerId);
                    switch (message.getMessageId())
                    {
                        case choke:
                            choked = true;
                            break;

                        case unchoke:
                            choked = false;
                            break;

                        case piece:
                        {
                            requestPending = false;
                            string payload = message.getPayload();
                            int index = bytesToInt(payload.substr(0, 4));
                            int begin = bytesToInt(payload.substr(4, 4));
                            string blockData = payload.substr(8);
                            pieceManager->blockReceived(peerId, index, begin, blockData);
                            break;
                        }
                        case have:
                        {
                            string payload = message.getPayload();
                            int pieceIndex = bytesToInt(payload);
                            pieceManager->updatePeer(peerId, pieceIndex);
                            break;
                        }

                        default:
                            break;
                    }
                    if (!choked)
                    {
                        if (!requestPending)
                        {
                            requestPiece();
                        }
                    }
                }
            }
        }
        catch (exception &e)
        {
            closeSock();
            //cout << "An error occurred while downloading from peer " << peerId.c_str() << " - " << peer->ip.c_str() << endl;
            //cout << e.what() << endl;
        }
    }

}

PeerConnection::PeerConnection(
        string CLIENT_ID,
        string infoHash,
        PieceManager *pieceManager,
        SharedQueue<Peer*>* queue)
        : CLIENT_ID(CLIENT_ID), infoHash(infoHash), pieceManager(pieceManager), queue(queue) {}


//stops the downloading
void PeerConnection::stop() {
    terminated = true;
}

//creates connection with peer, by performing handshake, recieving a bitfield and sending interested message
bool PeerConnection::establishNewConnection() {
    try {
        performHandshake();
        receiveBitField();
        sendInterested();
        return true;
    }
    catch (const runtime_error &e) {
        //cout << "An error occurred while connecting with peer " << peer->ip.c_str() << endl;
        return false;
    }
}

// performing handshake with a peer, creates socket, sends handshake message, receives response and returns it
void PeerConnection::performHandshake() {
    //cout << "performing handshake" << endl;
    //cout << "peer ip : " << peer->ip << " port: " << peer->port << endl;
    try
    {
        // try to create sock
        sock = createConnection(peer->ip, peer->port);
    }
    catch (runtime_error &e)
    {
        throw runtime_error("Cannot connect to peer [" + peer->ip + "]");
    }
    //cout << "sock created" << endl;
    string handshakeMessage = createHandshakeMessage();
    //cout << "handshake message: " << handshakeMessage << endl;
    sendData(sock, handshakeMessage);
    //cout << "data sent" << endl;
    string response = receiveData(sock, handshakeMessage.length());
    //cout << "response: " << response << endl;
    if (response.empty()) {
        throw runtime_error("Receive handshake failed: no response");
    }
    peerId = response.substr(PEER_ID_STARTING_POS, HASH_LEN);
    string receivedInfoHash = response.substr(INFO_HASH_STARTING_POS, HASH_LEN);
    if ((receivedInfoHash == infoHash) != 0) {
        throw runtime_error("Perform handshake with peer " + peer->ip + " failed: mismatching info hash");
    }
    //cout << "sucess: handshake with peer " << peerId << endl;
}

// formats data to handshake message
string PeerConnection::createHandshakeMessage() {
    const string protocol = "BitTorrent protocol";
    stringstream buffer;
    buffer << (char) protocol.length();
    buffer << protocol;
    string reserved;
    for (int i = 0; i < 8; i++)
        reserved.push_back('\0');
    buffer << reserved;
    buffer << hexDecode(infoHash);
    buffer << CLIENT_ID;
    assert (buffer.str().length() == protocol.length() + 49);
    return buffer.str();
}

// Receives and reads the message which contains BitField from the peer.
void PeerConnection::receiveBitField() {
    // Receive BitField from the peer
    //cout << "Receiving BitField message from peer " << peer->ip.c_str() << " ..." << endl;
    message message = receiveMessage();
    if (message.getMessageId() != bitField)
        throw runtime_error("Receive BitField from peer: FAILED [Wrong message ID]");
    peerBitField = message.getPayload();

    // Informs the PieceManager of the BitField received
    pieceManager->addPeer(peerId, peerBitField);

    //cout << "Receive BitField from peer: SUCCESS" << endl;
}


//Send an Interested message to the peer.
void PeerConnection::sendInterested() {
    //cout << "Sending Interested message to peer " << peer->ip.c_str() << " ..." << endl;
    string interestedMessage = message(interested).toString();
    sendData(sock, interestedMessage);
    //cout << "Send Interested message: SUCCESS" << endl;
}

// receives message from peer, returns message class
message PeerConnection::receiveMessage(int bufferSize) const {
    string reply = receiveData(sock, 0);
    if (reply.empty())
        return message(keepAlive);
    auto messageId = (uint8_t) reply[0];
    string payload = reply.substr(1);
    //cout << "Received message with ID " << messageId << " from peer" << peer->ip.c_str() << endl;
    return message(messageId, payload);
}

//request a block from a peer, block is picked by PieceManager.
void PeerConnection::requestPiece() {
    Block *block;
    try {
       block = pieceManager->nextRequest(peerId);
    } catch (exception e) {
        //cout << "erorr: " << e.what() << endl;
        return;
    }
    if (!block)
        return;

    int payloadLength = 12;
    char temp[payloadLength];
    // Needs to convert little-endian to big-endian
    uint32_t index = htonl(block->piece);
    uint32_t offset = htonl(block->offset);
    uint32_t length = htonl(block->length);
    memcpy(temp, &index, sizeof(int));
    memcpy(temp + 4, &offset, sizeof(int));
    memcpy(temp + 8, &length, sizeof(int));
    string payload;
    for (int i = 0; i < payloadLength; i++)
        payload += (char) temp[i];

    stringstream info;
    info << "Sending Request message to peer " << peer->ip << " ";
    info << "[Piece: " << to_string(block->piece) << " ";
    info << "Offset: " << to_string(block->offset) << " ";
    info << "Length: " << to_string(block->length) << "]";
    //cout << info.str().c_str() << endl;
    string requestMessage = message(request, payload).toString();
    sendData(sock, requestMessage);
    requestPending = true;
    //cout << "Send Request message: SUCCESS" << endl;
}

//closes connection
void PeerConnection::closeSock()
{
    if (sock)
    {
        // Close socket
        //cout << "Closed connection at socket " << sock << endl;
        close(sock);
        sock = {};
        requestPending = false;
        // If the peer has been added to piece manager, remove it
        if (!peerBitField.empty())
        {
            peerBitField.clear();
            try {
                pieceManager->removePeer(peerId);
            } catch (exception e) {
                //cout << "error removing peer: " << e.what() << endl;
            }
        }
    }
}

