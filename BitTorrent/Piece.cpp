//
// credit: https://github.com/ss16118/torrent-client-cpp
//
// this class represents one piece of the torrent file.
// it contains a vector of blocks
// and relevant functions

#include <utility>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <cassert>
#include <crypto/sha1.h>

#include "Piece.h"
#include "utils.h"

Piece::Piece(int index, vector<Block*> blocks, string hashValue):
        index(index), hashValue(move(hashValue))
{
    this->blocks = move(blocks);
}


Piece::~Piece()
{
    for (Block* block : blocks)
        delete block;
}


void Piece::reset()
{
    for (Block* block : blocks)
        block->status = missing;
}


Block* Piece::nextRequest()
{
    for (Block* block : blocks)
    {
        if (block->status == missing)
        {
            block->status = pending;
            return block;
        }
    }
    return nullptr;
}

void Piece::blockReceived(int offset, string data)
{
    for (Block* block : blocks)
    {
        if (block->offset == offset)
        {
            block->status = retrieved;
            block->data = data;
            return;
        }
    }
    throw runtime_error(
            "Trying to complete a non-existing block " + to_string(offset) +
            " in piece " + to_string(index)
    );
}


bool Piece::isComplete()
{

    return all_of(blocks.begin(), blocks.end(),[](Block* block)
                       {
                           return block->status == retrieved;
                       }
    );
}


bool Piece::isHashMatching()
{
    string pieceHash = hexDecode(sha1(getData()));
    return pieceHash == hashValue;
}


string Piece::getData()
{
    assert(isComplete());
    stringstream data;
    for (Block* block : blocks)
        data << block->data;
    return data.str();
}
