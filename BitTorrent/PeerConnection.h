//
// Created by Patrik on 02.01.2023.
//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#ifndef SEMESTRALKA_PEERCONNECTION_H
#define SEMESTRALKA_PEERCONNECTION_H

#include <istream>
#include "PeerRetriever.h"
#include "PieceManager.h"
#include "message.h"
#include "connect.h"
#include "SharedQueue.h"

using namespace std;

class PeerConnection {
private:
    int sock{};
    bool choked = true;
    bool terminated = false;
    bool requestPending = false;
    const string CLIENT_ID;
    const string infoHash;
    vector<Peer *> peers;
    Peer* peer;
    PieceManager* pieceManager;
    string peerBitField;
    SharedQueue<Peer*>* queue;
    string peerId;
    void performHandshake();
    void receiveBitField();
    void closeSock();
    void sendInterested();
    string createHandshakeMessage();
    void requestPiece();
public:
    explicit PeerConnection(string CLIENT_ID, string  infoHash, PieceManager* pieceManager, SharedQueue<Peer*>* queue);
    ~PeerConnection();
    void start();
    bool establishNewConnection();
    void stop();
    message receiveMessage(int bufferSize = 0) const;
};


#endif //SEMESTRALKA_PEERCONNECTION_H
