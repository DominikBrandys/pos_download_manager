//
// Created by patrik on 1.1.2023.
//

#include <curl/curl.h>
#include "PeerRetriever.h"
#include "utils.h"


shared_ptr<BDictionary> PeerRetriever::decodeResponse(string response) {
    shared_ptr<BItem> decodedResponse = decode(response);
    shared_ptr<BDictionary> responseDict = dynamic_pointer_cast<BDictionary>(decodedResponse);
    return responseDict;
}


PeerRetriever::PeerRetriever(string url, Torrent torrent) {
    this->response = announce(move(url));
}

// this class is responsible for retrieving a list of peers from a tracker server.
// only works with http trakcers, not udp ones
// returns vector of peers
vector<Peer *> PeerRetriever::retrievePeers(unsigned long downloadedBytes) {
    if (this->response.empty()) {
        throw runtime_error("No response");
    }
    shared_ptr<BDictionary> responseDict = decodeResponse(this->response);
    //cout << this->response << endl;
    shared_ptr<BItem> peersValue = responseDict->getValue("peers");

    shared_ptr<BList> peersList = dynamic_pointer_cast<BList>(peersValue);

    //cout << getPrettyRepr(responseDict) << endl;

    vector<Peer *> peers;
    if (!peersValue) {
        throw runtime_error("Response returned by the tracker is not in the correct format. ['peers' not found]");
    }

    if (typeid(*peersValue) == typeid(BString)) {
        // Unmarshalls the peer information:
        // Every 6 bytes represent a single peer with the first 4 bytes being the IP and the last 2 bytes being the port number.
        //cout << "peers received as a string" << endl;
        const int peerInfoSize = 6;
        string peersString = dynamic_pointer_cast<BString>(peersValue)->value();
        if (peersString.length() % peerInfoSize != 0)
            throw runtime_error("Received malformed 'peers' from tracker. ['peers' length needs to be divisible by 6]");

        const int peerNum = peersString.length() / peerInfoSize;
        for (int i = 0; i < peerNum; i++) {
            int offset = i * peerInfoSize;
            stringstream peerIp;
            peerIp << to_string((uint8_t) peersString[offset]) << ".";
            peerIp << to_string((uint8_t) peersString[offset + 1]) << ".";
            peerIp << to_string((uint8_t) peersString[offset + 2]) << ".";
            peerIp << to_string((uint8_t) peersString[offset + 3]);
            int peerPort = bytesToInt(peersString.substr(offset + 4, 2));
            Peer *newPeer = new Peer{peerIp.str(), peerPort};
            peers.push_back(newPeer);
        }
    }
        // Handles the second case where peer information is stored in a list
    else if (typeid(*peersValue) == typeid(BList)) {
        //cout << "peers received as a list" << endl;
        for (auto &item: *peersList) {
            // vytvorenie kniznice pre item (peer)
            shared_ptr<BDictionary> peerDict = dynamic_pointer_cast<BDictionary>(item);
            //dostan ip + error handling
            shared_ptr<BItem> tempPeerIp = peerDict->getValue("ip");
            if (!tempPeerIp)
                throw runtime_error("Received malformed 'peers' from tracker. [Item does not contain key 'ip']");
            //dostan port + error handling
            shared_ptr<BItem> tempPeerPort = peerDict->getValue("port");
            if (!tempPeerPort)
                throw runtime_error("Received malformed 'peers' from tracker. [Item does not contain key 'port']");

            string peerIp = dynamic_pointer_cast<BString>(tempPeerIp)->value();
            int peerPort = (int) dynamic_pointer_cast<BInteger>(tempPeerPort)->value();
            //vytvor peer struct
            Peer *newPeer = new Peer{peerIp, peerPort};
            peers.push_back(newPeer);
        }
    } else {
        throw runtime_error(
                "Response returned by the tracker is not in the correct format. ['peers' has the wrong type]");
    }
    for (const Peer *peer: peers) {
        //cout << "IP: " << peer->ip << ", Port: " << peer->port << endl;
    }
    return peers;
}

int PeerRetriever::getInterval() {
    return this->interval;
}

int PeerRetriever::getComplete() {
    return this->complete;
}

int PeerRetriever::getIncomplete() {
    return this->incomplete;
}

//sends annoucne request to http tracker and returns response
string PeerRetriever::announce(string url) {
    CURL *curl;
    CURLcode res;
    string result;
    curl = curl_easy_init();

    //cout << "sending announce request" << endl;

    if(curl) {
        res = curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        if (res != CURLE_OK) {
            //fprintf(stderr, "curl_easy_setop failed: cannot set curl url \n");
        }
        res = curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        if (res != CURLE_OK) {
            //fprintf(stderr, "curl_easy_setop failed: followlocation failed \n");
        }
        res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        if (res != CURLE_OK) {
            //fprintf(stderr, "curl_easy_setop failed: writefunction \n");
        }
        res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
        res = curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15L);
        if (res != CURLE_OK) {
            //fprintf(stderr, "curl_easy_setop failed: cannot set curl write data \n");
        }

        res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            //fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
        /* always clean up */
        curl_easy_cleanup(curl);
    } else {
        //fprintf(stderr, "curl request failed");
        return "";
    }
    return result;
}

PeerRetriever::~PeerRetriever() = default;

