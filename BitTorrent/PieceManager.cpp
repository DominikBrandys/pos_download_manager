//
// Created by Patrik on 02.01.2023.
//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#include <algorithm>
#include <cmath>
#include "PieceManager.h"

#define MAX_PENDING_TIME 5          // 5 sec
#define BLOCK_SIZE 16384              // 2 ^ 14
#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

PieceManager::PieceManager(vector<string> pieces, string pieceLength, string fileLength, int threadNum, string downloadPath) {
    this->pieceLength = stol(pieceLength);
    this->maximumConnections = threadNum;
    this->fileLength = stol(fileLength);
    this->missingPieces = processPieces(pieces);
    // Creates the destination file with the file size specified in the Torrent file
    downloadedFile.open(downloadPath, ios::binary | ios::out);
    downloadedFile.seekp(stol(fileLength) - 1);
    downloadedFile.write("", 1);
}

PieceManager::~PieceManager() {
    for (Piece* piece: missingPieces)
        delete piece;

    for (Piece *piece : ongoingPieces)
        delete piece;


}

Block *PieceManager::nextRequest(string peerId) {
    lock.lock();
    if (missingPieces.empty())
    {
        lock.unlock();
        return nullptr;
    }

    if (peers.find(peerId) == peers.end())
    {
        lock.unlock();
        return nullptr;
    }

    Block* block = expiredRequest(peerId);
    if (!block)
    {
        block = nextOngoing(peerId);
        if (!block)
            block = getRarestPiece(peerId)->nextRequest();
    }
    lock.unlock();

    return block;

//    lock.lock();
//
//    // If peer has no connection, end
//    if (peers.find(peerId) == peers.end())
//    {
//        lock.unlock();
//        return nullptr;
//    }
//
//    //cout << "looking for next pending block" << endl;
//
//    Block* block = nextPendingBlock(peerId);
//    if (!block) {
//        throw(runtime_error("no block found"));
//    }
//    lock.unlock();
//    return block;
}

Block* PieceManager::nextOngoing(string peerId)
{
    for (Piece* piece : ongoingPieces)
    {
        if (hasPiece(peers[peerId], piece->index))
        {
            Block* block = piece->nextRequest();
            if (block)
            {
                auto currentTime = time(nullptr);
                auto newPendingRequest = new PendingRequest;
                newPendingRequest->block = block;
                newPendingRequest->timestamp = time(nullptr);
                pendingRequests.push_back(newPendingRequest);
                return block;
            }
        }
    }
    return nullptr;
}

Block* PieceManager::expiredRequest(string peerId)
{
    time_t currentTime = time(nullptr);
    for (PendingRequest* pending : pendingRequests)
    {
        if (hasPiece(peers[peerId], pending->block->piece))
        {
            // If the request has expired
            auto diff = difftime(currentTime, pending->timestamp);
            if (diff >= MAX_PENDING_TIME)
            {
                // Resets the timer for that request
                pending->timestamp = currentTime;
                return pending->block;
            }
        }
    }
    return nullptr;
}

Piece* PieceManager::getRarestPiece(string peerId)
{
    // Custom comparator to make sure that the map is ordered by the index of the Piece.
    auto comp = [](const Piece* a, const Piece* b) { return a->index < b->index; };
    map<Piece*, int, decltype(comp)> pieceCount(comp);
    for (Piece* piece : missingPieces)
    {
        // If a connection has been established with the peer
        if (peers.find(peerId) != peers.end())
        {
            if (hasPiece(peers[peerId], piece->index))
                pieceCount[piece] += 1;
        }
    }

    Piece* rarest;
    int leastCount = INT16_MAX;
    for (auto const& [piece, count] : pieceCount)
    {
        if (count < leastCount)
        {
            leastCount = count;
            rarest = piece;
        }
    }

    missingPieces.erase(
            remove(missingPieces.begin(), missingPieces.end(), rarest),
            missingPieces.end()
    );
    ongoingPieces.push_back(rarest);
    return rarest;
}

unsigned long PieceManager::bytesDownloaded() {
    return 0;
}


bool PieceManager::isComplete() {
    lock.lock();
    bool isComplete = completedPieces.size() == totalPieces;
    lock.unlock();
    return isComplete;
}

vector<Piece *> PieceManager::processPieces(vector<string> pieceHashes) {
    vector<Piece *> pieces;
    int blockCount = ceil(pieceLength / BLOCK_SIZE);
    long remLength = pieceLength;
    totalPieces = pieceHashes.size();
    //cout << "File size: " << fileLength << endl;
    for (int i = 0; i < totalPieces; i++)
    {
        // The final piece is likely to have a smaller size.
        if (i == totalPieces - 1)
        {
            remLength = fileLength % pieceLength;
            blockCount = max((int) ceil((double)remLength / BLOCK_SIZE), 1);
        }
        vector<Block*> blocks;
        blocks.reserve(blockCount);

        for (int offset = 0; offset < blockCount; offset++)
        {
            auto block = new Block;
            block->piece = i;
            block->status = missing;
            block->offset = offset * BLOCK_SIZE;
            int blockSize = BLOCK_SIZE;
            if (i == totalPieces - 1 && offset == blockCount - 1)
                blockSize = remLength % BLOCK_SIZE;
            block->length = blockSize;
            blocks.push_back(block);
        }
        auto piece = new Piece(i, blocks, pieceHashes[i]);
        pieces.emplace_back(piece);
    }
    return pieces;
}

void PieceManager::blockReceived(string peerId, int pieceIndex, int blockOffset, string data) {
    //cout << "Received block " << blockOffset << " for piece << " << pieceIndex << " from peer " << peerId << endl;
    // Removes the received block from pending requests
    PendingRequest* requestToRemove = nullptr;
    lock.lock();
    for (PendingRequest* pending : pendingRequests)
    {
        if (pending->block->piece == pieceIndex && pending->block->offset == blockOffset)
        {
            requestToRemove = pending;
            break;
        }
    }

    pendingRequests.erase(
            remove(pendingRequests.begin(), pendingRequests.end(), requestToRemove),
            pendingRequests.end()
    );

    // Retrieves the Piece to which this Block belongs
    Piece* targetPiece = nullptr;
    for (Piece* piece : ongoingPieces)
    {
        if (piece->index == pieceIndex)
        {
            targetPiece = piece;
            break;
        }
    }
    lock.unlock();
    if (!targetPiece) {
        //cout << "Received Block does not belong to any ongoing Piece." << endl;
        throw runtime_error("Received Block does not belong to any ongoing Piece.");
    }
    targetPiece->blockReceived(blockOffset, move(data));
    //cout << "block received" << endl;
    if (targetPiece->isComplete())
    {
        // If the Piece is completed and the hash matches,
        // writes the Piece to disk
        if (targetPiece->isHashMatching())
        {
            write(targetPiece);
            // Removes the Piece from the ongoing list
            lock.lock();
            ongoingPieces.erase(
                    remove(ongoingPieces.begin(), ongoingPieces.end(), targetPiece),
                    ongoingPieces.end()
            );
            completedPieces.push_back(targetPiece);
            //piecesDownloadedInInterval++;
            lock.unlock();

//            stringstream info;
//            info << "(" << fixed << setprecision(2) << (((float) completedPieces.size()) / (float) totalPieces * 100) << "%) ";
//            info << to_string(completedPieces.size()) + " / " + to_string(totalPieces) + " Pieces downloaded...";
//            cout << info.str().c_str() << endl;
        }
        else
        {
            targetPiece->reset();
            //cout <<"Hash mismatch for Piece " << targetPiece->index << endl;
        }
    }

}


Block *PieceManager::nextPendingBlock(string peerId) {
    if (ongoingPieces.empty()) {
        //cout << "pushing back a missing piece" << endl;
        Piece* piece = getNextPiece(peerId);
        //if the peer doesnt have any missing pieces, end;
        if (!piece) {
            return nullptr;
        }
        ongoingPieces.push_back(piece);
        missingPieces.erase(missingPieces.begin());
    }

    Piece* piece = ongoingPieces.front();
    //cout << "piece index: " << piece->index << endl;
    for (auto block : piece->blocks) {
        ////cout << "block: " << piece->index << " offset: " << block->offset << " status: " << block->status << endl;
        if (block->status == missing) {
            //cout << "Requesting block: piece index: " << piece->index << " offset: " << block->offset << endl;
            block->status = pending;
            return block;
        }
    }
    return nullptr;
}

void PieceManager::addPeer(const string &peerId, string bitField) {
    lock.lock();
    peers[peerId] = bitField;
    lock.unlock();
    //cout << "Number of connections: " << peers.size() << "/" + to_string(maximumConnections) << endl;
}

void PieceManager::removePeer(const string& peerId)
{
    //cout << "removing peer" << endl;

    if (isComplete())
        return;


    auto iter = peers.find(peerId);
    if (iter != peers.end())
    {
        peers.erase(iter);
        stringstream info;
        //cout << "Number of connections: " << peers.size() << "/" + to_string(maximumConnections) << endl;
    }
    else
    {
        throw runtime_error("Attempting to remove a peer " + peerId + " with whom a connection has not been established.");
    }
}

void PieceManager::updatePeer(const string& peerId, int index)
{
    lock.lock();
    if (peers.find(peerId) != peers.end())
    {
        setPiece(peers[peerId], index);
        lock.unlock();
    }
    else
    {
        lock.unlock();
        throw runtime_error("Connection has not been established with peer " + peerId);
    }
}

// Bitfield operation that reads peers bitfield and tells if the peer has a particular piece
bool PieceManager::hasPiece(const string& bitField, int index)
{
    int byteIndex = floor(index / 8);
    int offset = index % 8;
    return (bitField[byteIndex] >> (7 - offset) & 1) != 0;
}

// Bitfield opperation that sets 1 at the index, reflects a have message from peer
void PieceManager::setPiece(string& bitField, int index)
{
    int byteIndex = floor(index / 8);
    int offset = index % 8;
    bitField[byteIndex] |= (1 << (7 - offset));
}

Piece *PieceManager::getNextPiece(string peerId) {
    for (Piece* piece : missingPieces)
    {
        // If a connection has been established with the peer
        if (peers.find(peerId) != peers.end())
        {
            if (hasPiece(peers[peerId], piece->index))
                return piece;
        }
    }
    return nullptr;
}

void PieceManager::write(Piece* piece)
{
    long position = piece->index * pieceLength;
    downloadedFile.seekp(position);
    downloadedFile << piece->getData();
}