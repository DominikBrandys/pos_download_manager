//
// credit: https://github.com/ss16118/torrent-client-cpp
//
// this class contains useful methods that function for data manipulation
//

#include <iostream>
#include <iomanip>
#include "../lib/bencode/bencoding.h"

using namespace std;
using namespace bencoding;
#ifndef SEMESTRALKA_UTILS_H
#define SEMESTRALKA_UTILS_H


string hexDecode(const string& value);

string urlEncode(const string &value);

int bytesToInt(string bytes);

string getValue(shared_ptr<BDictionary> rootDict, string key);

size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);

#endif //SEMESTRALKA_UTILS_H
