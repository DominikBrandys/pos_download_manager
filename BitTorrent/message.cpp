//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#include "message.h"
#include <iostream>
#include <sstream>
#include <bitset>


message::message(const uint8_t id, const string& payload):
        id(id), payload(payload), messageLength(payload.length() + 1) {}

string message::toString() {
    stringstream buffer;
    char* messageLengthAddr = (char*) &messageLength;
    string messageLengthStr;
    // Bytes are pushed in reverse order, assuming the data
    // is stored in little-endian order locally.
    for (int i = 0; i < 4; i++)
        messageLengthStr.push_back((char) messageLengthAddr[3 - i]);
    buffer << messageLengthStr;
    buffer << (char) id;
    buffer << payload;
    return buffer.str();
}


uint8_t message::getMessageId() const {
    return id;
}


string message::getPayload() const {
    return payload;
}