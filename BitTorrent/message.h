//
// credit: https://github.com/ss16118/torrent-client-cpp
//

#ifndef SEMESTRALKA_MESSAGE_H
#define SEMESTRALKA_MESSAGE_H

#include "istream"

using namespace std;

enum MessageType {
    keepAlive = -1,
    choke = 0,
    unchoke = 1,
    interested = 2,
    notInterested = 3,
    have = 4,
    bitField = 5,
    request = 6,
    piece = 7,
    cancel = 8,
    port = 9
};

class message {
private:
    const uint32_t messageLength;
    const uint8_t id;
    const string payload;
public:
    explicit message(uint8_t id, const string& payload = "");
    string toString();
    uint8_t getMessageId() const;
    string getPayload() const;
};


#endif //SEMESTRALKA_MESSAGE_H
