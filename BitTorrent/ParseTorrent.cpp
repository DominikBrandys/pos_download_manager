//
// Created by Patrik on 27.12.2022.
//
#include "ParseTorrent.h"
#include "utils.h"
#include "BitTorrentClient.h"
#include <curl/curl.h>

using namespace std;
using namespace bencoding;


ParseTorrent::ParseTorrent(string CLIENT_ID) : CLIENT_ID(CLIENT_ID) {}

ParseTorrent::~ParseTorrent() {
}

string buildURL(Torrent torrent);


//create hash with info about torrent
string getInfoHash(shared_ptr<BDictionary> rootDict) {
    shared_ptr<BItem> infoDictionary = rootDict->getValue("info");
    string infoString = encode(infoDictionary);
    string infoHash = sha1(infoString);
    return infoHash;
}

//divide pieces into parts of equal length of 20 bytes
vector<string> splitPieceHashes(shared_ptr<BDictionary> rootDict) {
    shared_ptr<BItem> piecesValue = rootDict->getValue("pieces");
    if (!piecesValue)
        throw runtime_error("Torrent file is malformed. [File does not contain key 'pieces']");
    string pieces = dynamic_pointer_cast<BString>(piecesValue)->value();

    vector<string> pieceHashes;

    int hash_length = 20;

    assert(pieces.size() % hash_length == 0);
    int piecesCount = (int) pieces.size() / hash_length;
    pieceHashes.reserve(piecesCount);
    for (int i = 0; i < piecesCount; i++)
        pieceHashes.push_back(pieces.substr(i * hash_length, hash_length));
    return pieceHashes;
}

//create torrent struct and set its parameters
Torrent ParseTorrent::buildTorrent(shared_ptr<BDictionary> rootDict) {
    Torrent torrent;
    torrent.Announce = getValue(rootDict, "announce");
    torrent.InfoHash = getInfoHash(rootDict);
    torrent.PieceLength = getValue(rootDict, "piece length");
    torrent.Length = getValue(rootDict, "length");
    torrent.Name = getValue(rootDict, "name");
    torrent.PieceHashes = splitPieceHashes(rootDict);;
    return torrent;
}

//parse the torrent file and return url
string ParseTorrent::parse(string filePath) {
    Torrent torrent;
    try {
        //read and decode torrent file
        ifstream torrent_file(filePath, ifstream ::binary);
        if (torrent_file.is_open()) {
            //cout << "opening file: success" << endl;
        } else {
            //cout << "opening file: failure" << endl;
        }

        shared_ptr<BItem> decodedData = decode(torrent_file);
        shared_ptr<BDictionary> rootDict = dynamic_pointer_cast<BDictionary>(decodedData);
        //create torrent struct
        this->torrent = buildTorrent(rootDict);
        string url = buildURL(this->torrent);
        //cout << url << endl;

            return url;


    } catch (const DecodingError &ex) {
        // error during decoding
        cerr << "error: " << ex.what() << "\n";
        return "";
    }
}

//getter for torrent
Torrent ParseTorrent::getTorrent() {
    return this->torrent;
}

// builds url from request, the port is just set to 6881
string ParseTorrent::buildURL(Torrent torrent) {
    string infoHash = urlEncode(hexDecode(torrent.InfoHash));
    string url = torrent.Announce + "?info_hash=" + infoHash + "&peer_id=" + CLIENT_ID + "&port=" + to_string(8909) + "&uploaded=0&downloaded=0&left=" + torrent.Length + "&event=started&compact=1";
    return url;
}




