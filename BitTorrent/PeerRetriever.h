//
// Created by patrik on 1.1.2023.
//

#ifndef SEMESTRALKA_PEERRETRIEVER_H
#define SEMESTRALKA_PEERRETRIEVER_H


#include <iostream>
#include <vector>
#include "../lib/bencode/bencoding.h"
#include "ParseTorrent.h"

using namespace bencoding;
using namespace std;

struct Peer
{
    string ip;
    int port;

};

class PeerRetriever {
private:
    Torrent torrent;
    string response;
    shared_ptr<BDictionary> decodeResponse(string response);
    int complete;
    int incomplete;
    int interval;
    string encodeAnnounceRequest(string message);
    string announce(string url);
    string udpannounce(string url);
public:
    PeerRetriever(string url, Torrent torrent);
    vector<Peer*> retrievePeers(unsigned long downloadedBytes = 0);
    ~PeerRetriever();
    int getComplete();
    int getIncomplete();
    int getInterval();


};


#endif //SEMESTRALKA_PEERRETRIEVER_H
