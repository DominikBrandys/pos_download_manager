//
// credit: https://github.com/ss16118/torrent-client-cpp
//
#include "utils.h"
#include <bitset>

string urlEncode(const string &value) {
    ostringstream escaped;
    escaped.fill('0');
    escaped << hex;
    for (char c : value)
    {
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }
        escaped << uppercase;
        escaped << '%' << setw(2) << int((unsigned char) c);
        escaped << nouppercase;
    }
    return escaped.str();
}

string hexDecode(const string& value)
{
    int hashLength = value.length();
    string decodedHexString;
    for (int i = 0; i < hashLength; i += 2)
    {
        string byte = value.substr(i, 2);
        char c = (char) (int) strtol(byte.c_str(), nullptr, 16);
        decodedHexString.push_back(c);
    }
    return decodedHexString;
}

//Converts a series of bytes in a string format to an integer.
int bytesToInt(string bytes)
{
    string binStr;
    long byteCount = bytes.size();
    for (int i = 0; i < byteCount; i++)
        binStr += bitset<8>(bytes[i]).to_string();
    return stoi(binStr, 0, 2);
}

//vrati string hodnotu z rootDict, urcenu klucom
string getValue(shared_ptr<BDictionary> rootDict, string key) {

    shared_ptr<BItem> Dictionary = rootDict->getValue(key);
    string value = getPrettyRepr(Dictionary);
    if (value.front() == '"' && value.back() == '"') {
        // If so, remove them by creating a new string without the first and last characters
        return value.substr(1, value.length() - 2);
    }
    return value;
}

size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}