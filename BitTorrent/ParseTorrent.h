//
// Created by Patrik on 27.12.2022.
//

#include <iostream>
#include <fstream>
#include <cassert>
#include <iomanip>
#include "../lib/bencode/bencoding.h"
#include "../lib/crypto/sha1.h"
#include "utils.h"

#ifndef POS_DM_BENCODING_H
#define POS_DM_BENCODING_H

struct Torrent
{
    string Announce;
    string InfoHash;
    vector<string>  PieceHashes;
    string PieceLength;
    string Length;
    string Name;
};

class ParseTorrent {
private:
    Torrent torrent;
    const string CLIENT_ID;
    string buildURL(Torrent torrent);
public:
    explicit ParseTorrent(string CLIENT_ID);
    ~ParseTorrent();
    string parse(string filePath);
    Torrent getTorrent();

    Torrent buildTorrent(shared_ptr<BDictionary> sharedPtr);
};


#endif //POS_DM_BENCODING_H
